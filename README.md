# General

The code and data that was used to write the paper "Formalizing cognitive biases in diagnostic reasoning" at the workshop on formal and cognitive reasoning ([FCR-2022](https://www.fernuni-hagen.de/wbs/fcr2022.html)) that took place at the German conference on artificial intelligence in September 2022.

# How to install

First clone this repository and create a conda environment. If you don't have conda installed on your system, I recommend to install [miniconda](https://docs.conda.io/en/latest/miniconda.html). The paper uses an adapted version of the `pomdp_py` library, which is installed from the `scs` conda channel. You will thus need to set that channel up first by adding

```
custom_channels:
  scs: https://scs.techfak.uni-bielefeld.de/condapkg
channels:
  - scs
  - conda-forge
  - defaults
```

to your `~/.condarc`. Then you can setup everything via

```sh
git clone git@gitlab.ub.uni-bielefeld.de:scs/trr-318-c05/papers/2022-07-workshop-formal-and-cognitive-reasoning.git
cd 2022-07-workshop-formal-and-cognitive-reasoning
conda env create
conda activate fcr-2022
```

Now you should be ready to go! :)

# How to run

After installing the code and activating the environment as described above, run the diagnostic loop via:

```sh
python main.py
```

You can adapt the bias of the agent in the `main.py` file, possible options are displayed in the top docstring. The diagnostic trajectory is saved as `.csv` file to your disk. To plot anything meaningful from that, use the following command:

```sh
python plot.py
```

Again, you can adjust the plots to produce in the `plot.py` file.
