"""
Run the diagnostic process model.

Possible diagnoses

    epileptic-seizure
    psychogenic-seizure
    syncope

Possible biases

    None
    Bias(Bias.PREMATURE_CLOSURE)
    Bias(Bias.CONFIRMATION_BIAS)
    Bias(Bias.AVAILABILITY_BIAS, target="epileptic-seizure")
    Bias(Bias.AVAILABILITY_BIAS, target="psychogenic-seizure")
    Bias(Bias.AVAILABILITY_BIAS, target="syncope")
"""

import logging
import os
import sys

from pomdp.domain import Bias
from pomdp.problem import POMDP, run_pouct

logging.basicConfig(
    stream=sys.stdout,
    level=logging.DEBUG,
    format="%(levelname)-5s  %(name)-15s  %(message)s"
)

diagnosis = "epileptic-seizure"
bias = None
problem = POMDP(true_diagnosis=diagnosis, bias=bias)
run_pouct(problem, num_sims=200, exploration_const=1.0, max_depth=50)

# diagnosis = "psychogenic-seizure"
# bias = Bias(Bias.PREMATURE_CLOSURE)
# b = "bias_None" if bias is None else f"{bias.type}_{bias.target}"
# folder = os.path.join("out", diagnosis, b)
# os.makedirs(folder, exist_ok=True)
# for i in range(10):
#    problem = POMDP(
#        true_diagnosis=diagnosis,
#        bias=bias
#    )
#    run_pouct(problem, num_sims=200, exploration_const=1.0, max_depth=50,
#              out_folder=folder)
