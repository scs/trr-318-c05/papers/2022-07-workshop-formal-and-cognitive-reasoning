import os

from pgmpy.inference import VariableElimination
from pgmpy.readwrite import BIFReader


class BayesianNetwork(object):

    @classmethod
    def from_bif(cls, filepath):
        # print(f"Reading bayesian network from {filepath}... ", end="")
        if filepath.endswith(".bif") and os.path.isfile(filepath):
            model = BIFReader(filepath).get_model()
            # print(f"Done. #nodes: {len(model.nodes())} #edges: {len(model.edges())}")
            return cls(model)
        else:
            raise ValueError(f"Expected .bif filepath to Bayesian network, but "
                             f"got {filepath}")

    def __init__(self, model):
        self._model = model
        self._model.check_model()
        self._inference = VariableElimination(self._model)

    @property
    def risk_factors(self) -> list[str]:
        return [n for n in self._model.nodes() if n.startswith("R-")]

    @property
    def diagnoses(self) -> list[str]:
        return self._model.states["diagnosis"]

    @property
    def symptoms(self) -> list[str]:
        return [n for n in self._model.nodes() if n.startswith("S-")]

    @property
    def query_variables(self):
        return [n for n in self._model.nodes() if n != "diagnosis"]

    @property
    def query_variable_states(self):
        for variable, values in self._model.states.items():
            if variable != "diagnosis":
                for value in values:
                    yield variable, value

    def values(self, variable: str):
        if variable not in self.query_variables:
            raise ValueError(f"Expected query variable, but got {variable}")
        return self._model.states[variable]

    def infer(self, variable: str, value: str, evidence: dict) -> float:
        """
        Infer the probability of the given variable having the given value given
        the evidence.
        """
        res = self._inference.query(variables=[variable],
                                    evidence=evidence,
                                    show_progress=False)
        res.normalize()
        return res.get_value(**{variable: value})

    def sample(self, variable: str, evidence: dict) -> str:
        """
        Sample a value for the variable given the evidence.
        """
        res = self._inference.query(variables=[variable],
                                    evidence=evidence,
                                    show_progress=False)
        data_frame = res.sample(1)  # data frame with one row and one col
        return data_frame[variable][0]

    def plot(self, filepath=None):
        node_positions = self._compute_node_plot_positions()
        d = self._model.to_daft(node_pos=node_positions,
                                pgm_params={"aspect": 5.0})
        if filepath is None:
            d.show()
        else:
            print(f"Plotting bayesian network to {filepath}... ", end="")
            d.render()
            d.savefig(filepath)
            print("Done")

    def _compute_node_plot_positions(self, row_offset=1, col_offset=3):
        risk_factors = {n: (i * col_offset, 0)
                        for i, n in enumerate(self.risk_factors)}
        symptoms = {n: (i * col_offset, -row_offset * 2)
                    for i, n in enumerate(self.symptoms)}
        mid_index = (max(len(risk_factors), len(symptoms)) - 1) / 2.0
        diagnoses = {"diagnosis": (mid_index * col_offset, -row_offset)}
        return {**risk_factors, **diagnoses, **symptoms}
