import logging
import math
import os.path
import time
import typing
from datetime import datetime

import pandas as pd
import pomdp_py
import pomdp_py.utils.interfaces.solvers
from pomdp_py import POUCT
from pomdp_py.utils import TreeDebugger

import pomdp.conf
import pomdp.domain
from pomdp.agent import Agent
from pomdp.environment import Environment
from pomdp.models import TransitionModel, ObservationModel, RewardModel, \
    PolicyModel, GeneratorModel
from pomdp.solver import POMCP

log = logging.getLogger("Problem")


class POMDP:

    @staticmethod
    def initial_particle_belief(n_particles):
        n_diagnoses = len(pomdp.conf.DIAGNOSES)
        n_particles_per_diagnosis = round(n_particles / float(n_diagnoses))
        return pomdp_py.Particles([
            pomdp.domain.State(diagnosis, None)
            for diagnosis in pomdp.conf.DIAGNOSES
            for _ in range(n_particles_per_diagnosis)
        ])

    @staticmethod
    def initial_histogram_belief():
        return pomdp_py.Histogram({
            pomdp.domain.State("epileptic-seizure", None): 1.0 / 3.0,
            pomdp.domain.State("psychogenic-seizure", None): 1.0 / 3.0,
            pomdp.domain.State("syncope", None): 1.0 / 3.0,
        })

    @staticmethod
    def initial_state(true_diagnosis):
        return pomdp.domain.State(true_diagnosis=true_diagnosis,
                                  pred_diagnosis=None)

    def __init__(self,
                 true_diagnosis: str,
                 # n_belief_particles: int,
                 bias: pomdp.domain.Bias | None = None):
        if true_diagnosis not in pomdp.conf.DIAGNOSES:
            raise ValueError(f"Expected diagnosis from {pomdp.conf.DIAGNOSES}, "
                             f"but got '{true_diagnosis}'")

        initial_belief = POMDP.initial_histogram_belief()
        # initial_belief = POMDP.initial_particle_belief(n_belief_particles)
        initial_state = POMDP.initial_state(true_diagnosis)

        generator_model = GeneratorModel(bias)
        self.agent = Agent(initial_belief,
                           PolicyModel(),
                           blackbox_model=generator_model)
        generator_model.agent = self.agent

        self.env = Environment(initial_state,
                               TransitionModel(),
                               ObservationModel(),
                               RewardModel())


def run_rollout(problem: POMDP,
                num_sims=100,
                max_depth=5,
                discount_factor=0.9):
    planner = pomdp_py.PORollout(num_sims=num_sims,
                                 max_depth=max_depth,
                                 discount_factor=discount_factor,
                                 rollout_policy=problem.agent.policy_model,
                                 particles=True,
                                 action_prior=None)
    run(problem, planner)


def run_pomcp(problem: POMDP,
              max_depth=5,
              planning_time=-1.,
              num_sims=10,
              discount_factor=0.9,
              exploration_const=math.sqrt(2),
              num_visits_init=0,
              value_init=0,
              show_progress=False,
              pbar_update_interval=5):
    planner = POMCP(max_depth=max_depth, planning_time=planning_time,
                    num_sims=num_sims, discount_factor=discount_factor,
                    exploration_const=exploration_const,
                    num_visits_init=num_visits_init,
                    value_init=value_init, show_progress=show_progress,
                    pbar_update_interval=pbar_update_interval,
                    rollout_policy=problem.agent.policy_model,
                    action_prior=None)
    run(problem, planner)


def run_pouct(problem: POMDP,
              max_depth=5,
              planning_time=-1.,
              num_sims=10,
              discount_factor=0.9,
              exploration_const=math.sqrt(2),
              show_progress=False,
              out_folder=""):
    planner = POUCT(max_depth=max_depth, num_sims=num_sims,
                    discount_factor=discount_factor, exploration_const=exploration_const,
                    rollout_policy=problem.agent.policy_model, show_progress=show_progress)
    run(problem, planner, out_folder)


def run(problem, planner, out_folder=""):
    done = False
    n_steps = 0
    trajectory = {
        "belief_epileptic-seizure": [],
        "belief_psychogenic-seizure": [],
        "belief_syncope": [],
        "queried_feature": [],
        "observed_feature_value": [],
        "observed_reward": [],
        "runtime": []
    }

    while not done:
        n_steps += 1

        temp_belief = problem.agent.diagnostic_belief
        trajectory["belief_epileptic-seizure"].append(temp_belief["epileptic-seizure"])
        trajectory["belief_psychogenic-seizure"].append(temp_belief["psychogenic-seizure"])
        trajectory["belief_syncope"].append(temp_belief["syncope"])

        start = time.time()
        action = planner.plan(problem.agent)
        # TreeDebugger(problem.agent.tree).pp
        observation, reward, done = problem.env.step(action)
        problem.agent.update_belief(action, observation)
        problem.agent.update_history(action, observation)
        planner.update(problem.agent, action, observation)
        stop = time.time()

        trajectory["queried_feature"].append(action.target)
        trajectory["observed_feature_value"].append(observation.value)
        trajectory["observed_reward"].append(reward)
        trajectory["runtime"].append(stop - start)

    df = pd.DataFrame.from_dict(trajectory)
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    path = f"{timestamp}_{problem.env.state.true_diagnosis}_transitions.csv"
    path = os.path.join(out_folder, path)
    log.info(f"Saving trajectory to '{path}'")
    df.to_csv(path, index=False)
