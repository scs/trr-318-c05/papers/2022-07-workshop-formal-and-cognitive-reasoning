import logging

import pomdp_py

from pomdp.conf import NETWORK
from pomdp.domain import State, Action
from pomdp.models.generator_model import normalized_entropy

log = logging.getLogger("Agent")


class Agent(pomdp_py.Agent):

    def __init__(self, init_belief, policy_model, transition_model=None,
                 observation_model=None, reward_model=None, blackbox_model=None):
        super().__init__(init_belief, policy_model, transition_model,
                         observation_model, reward_model, blackbox_model)
        log.debug(f"Initial belief = {self.format_belief()}")

    def set_belief(self, belief, prior=False):
        super().set_belief(belief, prior)
        log.debug(f"Belief = {self.format_belief()}")

    def update_belief(self, action, observation):
        if action.type == Action.SUBMIT:
            log.debug("Skipping belief update after submit action")
            return

        new_belief = {}
        total = 0.0
        for state in self.belief:
            observation_likelihood = self.blackbox_model.observation_probability(observation,
                                                                                 state,
                                                                                 action,
                                                                                 self.history,
                                                                                 self.diagnostic_belief)
            new_belief[state] = self.belief[state] * observation_likelihood
            total += new_belief[state]

        # normalize
        for state in new_belief:
            new_belief[state] /= total

        self.set_belief(pomdp_py.Histogram(new_belief))

    @property
    def diagnostic_belief(self):
        return {
            state.true_diagnosis: self.belief[state]
            for state in [
                State(diagnosis, None) for diagnosis in NETWORK.diagnoses
            ]
        }

    def format_belief(self):
        belief = self.diagnostic_belief
        return str(belief) + " - normalized entropy = {}".format(
            normalized_entropy(belief)
        )

    """
    @property
    def diagnostic_belief(self):
        particles = self.belief.condense().particles
        posteriors = {
            d: sum(
                p
                for state, p in particles
                if state.true_diagnosis == d
            )
            for d in NETWORK.diagnoses
        }
        return posteriors

    def format_belief(self):
        belief = self.diagnostic_belief
        return str(belief) + " - {} particles, {} distinct, relative entropy = {}".format(
            len(self.belief), len(self.belief.condense()), relative_entropy(belief)
        )
    """


"""
class Belief:

    def __init__(self, belief: Dict[State, float]):
        ...

    def update(self, observation: Observation):
        ...

    def sample(self) -> State:
        ...

    def relative_entropy(self) -> float:
        ...

    def argmax(self) -> State:
        ...

    def __str__(self):
        ...


class History:

    def __init__(self):
        self.events = []

    def __hash__(self):
        return hash(tuple(self.events))

    def add(self, action, observation):
        self.events.append((action, observation))


class Agent:

    def __init__(self,
                 initial_belief: Dict[State, float],
                 policy_model: PolicyModel,
                 observation_model: ObservationModel):
        self._history = History()
        self._belief = Belief(initial_belief)
        self._policy_model = policy_model
        self._observation_model = observation_model
        self._planner = POMCP(self)

    @property
    def history(self) -> History:
        return self._history

    def update_history(self, action: Action, observation: Observation):
        self._history.add(action, observation)

    @property
    def belief(self) -> Belief:
        return self._belief

    def update_belief(self, observation: Observation):
        self._belief.update(observation)

    def sample_belief(self) -> State:
        return self._belief.sample()

    def possible_actions(self, state: State) -> list[Action]:
        return self._policy_model.get_all_actions(state=state, history=self._history)

    def plan(self, env: Environment) -> Action:
        return self._planner.plan(env)
"""
