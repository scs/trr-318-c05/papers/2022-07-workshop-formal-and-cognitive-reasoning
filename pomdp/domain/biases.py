from pomdp.conf import DIAGNOSES


class Bias:
    PREMATURE_CLOSURE = "premature_closure"
    AVAILABILITY_BIAS = "availability_bias"
    CONFIRMATION_BIAS = "confirmation_bias"

    def __init__(self, bias_type: str, bias_target: str | None = None):
        if bias_target is not None and bias_target not in DIAGNOSES:
            raise ValueError(f"Expected diagnosis as availability "
                             f"bias target, but got {bias_target}")
        self.type = bias_type
        self.target = bias_target

    def __str__(self):
        return "<Bias type = {}, target = {}>".format(
            self.type, self.target
        )
