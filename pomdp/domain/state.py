import pomdp_py

from pomdp.conf import DIAGNOSES, QUERY_VARIABLES, QUERY_VARIABLE_STATES


class State(pomdp_py.State):
    """
    A data class to capture a state within the diagnostic process.
    """

    def __init__(self,
                 true_diagnosis: str,
                 pred_diagnosis: str | None):
        if true_diagnosis not in DIAGNOSES:
            raise ValueError(f"Expected true diagnosis from {DIAGNOSES}, but "
                             f"got {true_diagnosis}")
        if pred_diagnosis not in DIAGNOSES + [None]:
            raise ValueError(f"Expected predicted diagnosis from {DIAGNOSES} or "
                             f"None, but got {pred_diagnosis}")

        self.true_diagnosis = true_diagnosis
        self.pred_diagnosis = pred_diagnosis
        super().__init__()

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, other):
        return isinstance(other, State) and hash(self) == hash(other)

    def __repr__(self):
        return f"<State " \
               f"true_diagnosis = {self.true_diagnosis}, " \
               f"pred_diagnosis = {self.pred_diagnosis}>"

    @property
    def is_terminal(self):
        return self.pred_diagnosis is not None
