import pomdp_py

from pomdp.conf import DIAGNOSES, QUERY_VARIABLES


class Action(pomdp_py.Action):
    """
    A data class to capture an action within the diagnostic process.
    """
    QUERY = "query"
    SUBMIT = "submit"

    def __init__(self, action_type: str, action_target: str):
        if action_type not in [Action.QUERY, Action.SUBMIT]:
            raise ValueError(f"Expected query or submit action type, but got "
                             f"{action_type}")
        if action_type == Action.QUERY and action_target not in QUERY_VARIABLES:
            raise ValueError(f"Expected query variable from {QUERY_VARIABLES}, "
                             f"but got {action_target}")
        if action_type == Action.SUBMIT and action_target not in DIAGNOSES:
            raise ValueError(f"Expected diagnosis from {DIAGNOSES}, but got "
                             f"{action_target}")

        self.type = action_type
        self.target = action_target
        super().__init__()

    def __eq__(self, other):
        return isinstance(other, Action) and hash(self) == hash(other)

    def __hash__(self):
        return hash(repr(self))

    def __repr__(self):
        return f"<Action {self.type} {self.target}>"
