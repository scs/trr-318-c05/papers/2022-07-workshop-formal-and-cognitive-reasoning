import pomdp_py

from pomdp.conf import DIAGNOSES, QUERY_VARIABLES, QUERY_VARIABLE_STATES


class Observation(pomdp_py.Observation):
    """
    A data class to capture medical observations made during the diagnostic
    process. After query actions, the agent observes a value of a specific
    feature. After submit actions, the agent observes the end of the process.
    """

    def __init__(self, variable: str, value: str):
        is_query_observation = variable in QUERY_VARIABLES
        is_submit_observation = variable in DIAGNOSES
        if not is_query_observation and not is_submit_observation:
            raise ValueError(f"Expected variable from domain model, but got "
                             f"'{variable}'")
        if is_query_observation and value not in QUERY_VARIABLE_STATES[variable]:
            raise ValueError(f"Expected value of query variable '{variable}' "
                             f"to be in {QUERY_VARIABLE_STATES[variable]}, but "
                             f"got '{value}'")
        if is_submit_observation and value != "submitted":
            raise ValueError(f"Expected 'submitted' as observation value for "
                             f"diagnosis '{variable}', but got '{value}'")

        self.variable = variable
        self.value = value
        super().__init__()

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, other):
        return isinstance(other, Observation) and hash(self) == hash(other)

    def __repr__(self):
        return f"<Observation {self.variable} = {self.value}>"
