import logging

from pomdp.domain import State, Action, Observation
from pomdp.models import ObservationModel, RewardModel, TransitionModel

log = logging.getLogger("Environment")


class Environment:

    def __init__(self,
                 initial_state: State,
                 transition_model: TransitionModel,
                 observation_model: ObservationModel,
                 reward_model: RewardModel):
        self.state = initial_state
        self.transition_model = transition_model
        self.observation_model = observation_model
        self.reward_model = reward_model
        self.n_steps = 0
        self.history = ()
        log.debug(f"Initial state = {initial_state}")

    def reset(self, state: State):
        log.debug(f"Resetting environment to state = {state}")
        self.state = state
        self.n_steps = 0
        self.history = ()

    def step(self, action: Action) -> tuple[Observation, float, bool]:
        self.n_steps += 1
        log.debug(f"Performing step {self.n_steps}")

        next_state = self.transition_model.sample(self.state, action)
        observation = self.observation_model.sample(next_state, action, self.history)
        reward = self.reward_model.sample(self.state, action, next_state)
        done = next_state.is_terminal

        log.debug(f"\t State = {self.state}")
        log.debug(f"\t History = {self.history}")
        log.debug(f"\t Action = {action}")
        log.debug(f"\t Next state = {next_state}")
        log.debug(f"\t Reward = {reward}")
        log.debug(f"\t Observation = {observation}")

        self.state = next_state
        self.history += ((action, observation), )
        return observation, reward, done
