import logging

import pomdp_py

from pomdp.domain import State, Action

log = logging.getLogger("Reward model")


class RewardModel(pomdp_py.RewardModel):

    def sample(self,
               state: State,
               action: Action,
               next_state: State | None = None) -> float:
        """
        Sample the reward for transitioning from state s to state s' via action
        a.

        Args:
            state: The current state.
            action: The executed action.
            next_state: The successor state.

        Returns:
            The reward R(s, a, s')
        """
        if state.is_terminal:
            return 0.0

        if action.type == Action.QUERY:
            return -1.0

        if action.type == Action.SUBMIT:
            correct = action.target == state.true_diagnosis
            return 100.0 if correct else -100.0

        assert False, "This code is never reached"
