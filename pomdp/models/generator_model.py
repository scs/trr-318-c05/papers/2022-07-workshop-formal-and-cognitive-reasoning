import logging
import math
import random

import pomdp_py
import scipy.stats

from pomdp.conf import NETWORK
from pomdp.domain import Bias
from pomdp.domain import State, Action, Observation
from pomdp.models import TransitionModel, ObservationModel

log = logging.getLogger("Generator model")


def normalized_entropy(belief: dict) -> float:
    n_beliefs = len(belief)
    if n_beliefs == 1:
        return 0.0
    else:
        max_entropy = math.log(n_beliefs)
        current_entropy = scipy.stats.entropy(list(belief.values()))
        return current_entropy / max_entropy


class GeneratorModel(pomdp_py.BlackboxModel):
    """
    A generator used during POMCP to simulate trajectories. This model defines
    the subjective view of the agent over the environment. Thus, this is the
    preferred spot to insert biases into the planning process of the agent.
    """

    def __init__(self, bias: Bias | None = None):
        self.agent = None
        self.transition_model = TransitionModel()
        self.observation_model = ObservationModel()
        self.bias = bias
        self.entropy_threshold = 0.75 if bias is not None and bias.type == Bias.PREMATURE_CLOSURE else 0.25
        log.debug(f"Bias = {bias}")
        log.debug(f"Entropy threshold = {self.entropy_threshold}")
        super().__init__()

    def sample(self, state: State, action: Action, history: tuple) -> tuple[State, Observation, float, int]:
        """
        Args:
            state: The state of the environment prior to action execution.
            action: The executed action.

        Returns:
            A tuple (s', o, r, n_steps) containing the next state, the collected
            observation and the collected reward. n_steps is only added for
            compatibility reasons with pomdp_py and is set to 1 if a transition
            happened and 0 if no transition happened.
        """
        next_state = self.transition_model.sample(state, action)
        observation = self.sample_observation(action, next_state, history)
        reward = self.sample_reward(state, action)
        n_steps = 0 if state.is_terminal else 1
        return next_state, observation, reward, n_steps

    def sample_observation(self, action: Action, next_state: State, history: tuple) -> Observation:
        if action.type == Action.SUBMIT:
            return self.observation_model.sample(next_state, action, history)

        if action.type == Action.QUERY:
            variable = action.target
            observations = [Observation(variable, value) for value in NETWORK.values(variable)]
            # use environment observation model here because scaling is dependent on the diagnosis
            # and not on the observation value, i.e. scaling is equal for all observation probabilities
            # -> gain a speedup by skipping the step
            p = self.observation_model.probability(observations[0], next_state, action, history)
            weights = [p, 1.0 - p]
            sample = random.choices(observations, weights=weights, k=1)[0]
            return sample

        assert False, "This code is never reached"

    def observation_probability(self, observation: Observation, next_state: State, action: Action,
                                history: tuple, belief: dict):
        p = self.observation_model.probability(observation, next_state, action, history)
        if self.bias is not None and self.bias.type == Bias.AVAILABILITY_BIAS:
            factor = 0.4 if next_state.true_diagnosis == self.bias.target else 0.3
            p *= factor
        if self.bias is not None and self.bias.type == Bias.CONFIRMATION_BIAS:
            factor = belief[next_state.true_diagnosis]
            p *= factor
        return p

    def sample_reward(self, state: State, action: Action) -> float:
        if state.is_terminal:
            return 0.0

        if action.type == Action.QUERY:
            return -1.0

        if action.type == Action.SUBMIT:
            best_candidate = self.agent.belief.mpe().true_diagnosis
            reasonable = action.target == best_candidate
            justified = normalized_entropy(self.agent.diagnostic_belief) < self.entropy_threshold
            return 100.0 if reasonable and justified else -100.0

        assert False, "This code is never reached"
