import logging

import pomdp_py

from pomdp.conf import NETWORK
from pomdp.domain import State, Action, Observation

log = logging.getLogger("Observation model")


class ObservationModel(pomdp_py.ObservationModel):
    """
    The ObservationModel captures the likelihood of observations.

    If the doctor queries a medical feature, the observation model emits an
    observation of this feature value. Here, the likelihood describes the
    probability with which to observe a feature value like that given the
    already collected knowledge about the patient.

    Sampling an observation is therefore different from transitioning in the
    sense that the true diagnosis is not taken into account.

    If the doctor queries multiple features in a row, the observation model effectively
    samples a new patient each time using the medical domain model:

        P(f3, f2, f1 | d) * P(d) = P(f3 | f2, f1, d) * P(f2 | f1, d) * P(f1 | d) * P(d)
        P(f3, f2, f1 | d)        = P(f3 | f2, f1, d) * P(f2 | f1, d) * P(f1 | d)
    """

    def sample(self, next_state: State, action: Action, history: tuple) -> Observation:
        """
        Sample an observation obtained in the new state s' after executing
        action a.

        Args:
            next_state: The new state s' after executing action a.
            action: The executed action a.

        Returns:
            An observation obtained by executing a and transitioning to s'.
        """
        if next_state.is_terminal:
            return Observation(next_state.pred_diagnosis, "submitted")

        if action.type == Action.QUERY:
            variable = action.target
            knowledge = {
                observation.variable: observation.value
                for action, observation in history
                if action.type == Action.QUERY
            }
            value = NETWORK.sample(variable, evidence={**knowledge, "diagnosis": next_state.true_diagnosis})
            return Observation(variable, value)

        if action.type == Action.SUBMIT:
            diagnosis = action.target
            return Observation(diagnosis, "submitted")

        assert False, "This code is never reached"

    def probability(self, observation, next_state, action, history):
        if observation.variable in NETWORK.query_variables:
            knowledge = {
                observation.variable: observation.value
                for action, observation in history
                if action.type == Action.QUERY
            }
            return NETWORK.infer(observation.variable,
                                 observation.value,
                                 evidence={**knowledge, "diagnosis": next_state.true_diagnosis})
        else:
            raise NotImplementedError
