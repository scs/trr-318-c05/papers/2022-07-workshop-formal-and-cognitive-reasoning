import logging
import random

import pomdp_py

from pomdp.conf import NETWORK
from pomdp.domain import State, Action

log = logging.getLogger("Policy model")


class PolicyModel(pomdp_py.RolloutPolicy):

    def sample(self, state: State) -> Action:
        raise NotImplementedError

    def rollout(self, state: State, history=None) -> Action:
        actions = self.get_all_actions(state, history)
        return random.choice(actions)

    def get_all_actions(self,
                        state: State | None = None,
                        history: tuple | None = None) -> list[Action]:
        if state is None or history is None:
            raise NotImplementedError

        known_variables = [action.target
                           for action, _ in history
                           if action.type == Action.QUERY]
        queries = [Action(Action.QUERY, var)
                   for var in NETWORK.query_variables
                   if var not in known_variables]
        diagnoses = [
            Action(Action.SUBMIT, diagnosis)
            for diagnosis in NETWORK.diagnoses
        ]
        return queries + diagnoses
