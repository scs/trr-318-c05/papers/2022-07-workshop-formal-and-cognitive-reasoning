import copy
import logging

import pomdp_py

from pomdp.domain import State, Action

log = logging.getLogger("Transition model")


class TransitionModel(pomdp_py.TransitionModel):
    """
    The TransitionModel defines all state changes. A state change either increases
    the collected medical knowledge over the patient or ends the process with a
    submit action.
    """

    def sample(self, state: State, action: Action) -> State:
        """
        Sample next state from P(s' | s, a)
        """
        if not state.is_terminal and action.type == Action.SUBMIT:
            return State(true_diagnosis=state.true_diagnosis,
                         pred_diagnosis=action.target)

        return copy.deepcopy(state)

    def probability(self, next_state, state, action):
        """
        Compute P(s' | s, a)
        """
        return 1.0 if next_state == self.sample(state, action) else 0.0
