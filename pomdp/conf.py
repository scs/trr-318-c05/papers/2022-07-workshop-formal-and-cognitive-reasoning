import network

NETWORK = network.BayesianNetwork.from_bif("resources/network.bif")
# NETWORK = network.BayesianNetwork.from_bif("../../resources/network.bif")
DIAGNOSES = NETWORK.diagnoses
QUERY_VARIABLES = NETWORK.query_variables


def _collect_variable_states():
    states = {var: [] for var in QUERY_VARIABLES}
    for var, val in NETWORK.query_variable_states:
        states[var].append(val)
    return states


QUERY_VARIABLE_STATES = _collect_variable_states()
