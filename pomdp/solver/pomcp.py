import pomdp_py


class POMCP(pomdp_py.POMCP):

    def update(self, agent, real_action, real_observation,
               state_transform_func=None):
        super().update(agent, real_action, real_observation, state_transform_func)

        """
        belief = agent.belief.condense()
        if len(belief) < len(DIAGNOSES):
            # belief in at least one diagnosis died -> revive
            alive = [state.true_diagnosis for state in belief.values]
            dead = [d for d in DIAGNOSES if d not in alive]

            log.debug("Reviving belief in {} diagnoses: {}".format(
                len(dead), dead
            ))
            for diagnosis in dead:
                particle = agent.belief.random()
                new_particle = copy.deepcopy(particle)
                new_particle.true_diagnosis = diagnosis
                agent.belief.add(new_particle)

            log.debug(f"Belief = {agent.format_belief()}")

            # update tree with changed belief
            if agent.tree is not None:
                agent.tree.belief = copy.deepcopy(agent.belief)

            # assert equal knowledge and pred-diagnosis
        """
