import json
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import sklearn.metrics

from pomdp.conf import DIAGNOSES, QUERY_VARIABLES


# Generate LaTex compatible file to import in overleaf with correct font etc.
# matplotlib.use("pgf")
# matplotlib.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     'pgf.rcfonts': False,
# })


def plot_belief_change(filepath, out_file=None):
    df = pd.read_csv(filepath)
    df = df.rename(
        columns={
            "belief_epileptic-seizure": "Epileptic seizure",
            "belief_psychogenic-seizure": "Psychogenic seizure",
            "belief_syncope": "Syncope"
        }
    )
    df = df[["Epileptic seizure", "Psychogenic seizure", "Syncope", "queried_feature"]]
    ax = df.plot.bar(
        x="queried_feature",
        stacked=True,
        xlabel="Queried feature",
        ylabel="Diagnostic belief"
    )
    plt.tight_layout()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.legend(loc="upper right")

    # move x ticks between two belief changes and remove submit at the end
    x_ticks = (plt.xticks()[0] + 0.5)[:-1]
    labels = df["queried_feature"][:-1]
    ax.set_xticks(x_ticks, labels=labels)
    if out_file is not None:
        print(f"Saving plot to '{out_file}'")
        plt.savefig(out_file)
    else:
        plt.show()


def confusion_matrix(in_folder, out_file):
    true = []
    pred = []
    for file in os.listdir(in_folder):
        if not file.endswith(".csv"):
            print(f"Skipping non-csv file: {file}")
            continue

        true_diagnosis = file[20:-16]
        path = os.path.join(in_folder, file)
        df = pd.read_csv(path)
        pred_diagnosis = df["queried_feature"].iloc[-1]

        true.append(true_diagnosis)
        pred.append(pred_diagnosis)

    cm = sklearn.metrics.confusion_matrix(true, pred, labels=DIAGNOSES)
    print("Saving confusion matrix to " + out_file)
    with open(out_file, mode="w") as f:
        np.savetxt(
            f,
            cm.astype(int),
            fmt="%i",
            header=f"Diagnostic confusion matrix for diagnoses: {DIAGNOSES}"
        )


def plot_confusion_matrix(filepath, title, save=False):
    labels = ["Epileptic seizure", "Psychogenic seizure", "Syncope"]
    print(f"Assuming label ordering: {labels}")
    with open(filepath, mode="r") as f:
        matrix = np.loadtxt(f)
    ax = sn.heatmap(matrix, annot=True, cbar=False, cmap="Blues",
                    xticklabels=labels, yticklabels=labels)
    # ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    plt.xlabel("Predicted value", labelpad=15)
    plt.ylabel("True value", labelpad=15)
    plt.title(title)
    plt.tight_layout()
    if save:
        filename, _ = os.path.splitext(filepath)
        filename += ".png"
        print(f"Saving plot to '{filename}'")
        plt.savefig(filename)
    else:
        plt.show()


def exploration_behaviour(in_folder, out_file):
    # max 35 features to query
    queries = pd.DataFrame(index=range(1, 36), columns=QUERY_VARIABLES)
    queries.fillna(value=0, inplace=True)

    for file in os.listdir(in_folder):
        if not file.endswith(".csv"):
            print(f"Skipping non-csv file: {file}")
            continue
        else:
            print(f"Scanning {file}...", flush=True)

        path = os.path.join(in_folder, file)
        df = pd.read_csv(path)
        for timestep, feature in enumerate(df["queried_feature"], start=1):
            if feature not in DIAGNOSES:
                queries[feature][timestep] += 1

    print(f"Saving exploration statistics to {out_file}...")
    queries.to_csv(out_file)


def plot_exploration_behaviour(filepath):
    df = pd.read_csv(filepath, index_col=0)
    fig = plt.figure(figsize=(8, 8))
    sn.heatmap(df.transpose())
    plt.xlabel("Time step", labelpad=15)
    plt.ylabel("Queried feature", labelpad=15)
    plt.title("Exploration behaviour over n trajectories")
    plt.tight_layout()
    plt.show()


def feature_preference(in_folder, out_file):
    counts = {
        variable: 0 for variable in QUERY_VARIABLES
    }
    for file in os.listdir(in_folder):
        if not file.endswith(".csv"):
            print(f"Skipping non-csv file: {file}")
            continue
        else:
            print(f"Scanning {file}...", flush=True)

        path = os.path.join(in_folder, file)
        df = pd.read_csv(path)
        for feature in df["queried_feature"]:
            if feature not in DIAGNOSES:
                counts[feature] += 1

    print(f"Saving feature preference histogram to {out_file}...")
    with open(out_file, mode="w") as f:
        json.dump(counts, f, indent=4)


def plot_feature_preference(filepath):
    with open(filepath, mode="r") as f:
        content = json.load(f)
    labels = sorted([key for key in content], reverse=True)
    counts = [content[key] for key in labels]

    # plt.style.use('seaborn-dark')
    # plt.grid(True)
    fig = plt.figure(figsize=(8, 8))
    y_pos = np.arange(len(labels))
    plt.barh(y_pos, counts)
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    plt.yticks(y_pos, labels)
    plt.xlabel("Number of queries")
    plt.ylabel("Queries feature")
    plt.title("Queried features during diagnosis ")
    plt.tight_layout()
    # plt.show()
    plt.savefig("test.png", dpi=300)


def plot_feature_difference():
    with open("feature_counts_unbiased.json", mode="r") as f:
        content = json.load(f)
    labels = sorted([key for key in content], reverse=True)
    unbiased_counts = [content[key] for key in labels]
    data = {"No bias": unbiased_counts}

    # for x in ["ES", "PNES", "S"]:
    for x in ["ES"]:
        with open(f"feature_counts_availability_bias_{x}.json", mode="r") as f:
            content = json.load(f)
        biased_counts = [content[key] for key in labels]
        data[f"Availability bias ({x})"] = biased_counts

    df = pd.DataFrame(data, index=labels)
    df.plot.barh(xlabel="Queried feature", figsize=(8, 8))
    plt.tight_layout()
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    plt.show()
    # plt.savefig("availability_bias.eps")


def trajectory_length(in_folder):
    lenghts = []
    for file in os.listdir(in_folder):
        if not file.endswith(".csv"):
            print(f"Skipping non-csv file: {file}")
            continue

        path = os.path.join(in_folder, file)
        df = pd.read_csv(path)
        lenghts.append(len(df))

    print("Scanned {} files from {}".format(len(lenghts), in_folder))
    print("Median: {}".format(np.median(lenghts)))
    print("Mean: {}".format(np.mean(lenghts)))
    print("Std: {}".format(np.std(lenghts)))


if __name__ == "__main__":
    plot_belief_change("data/confirmation_bias/2022-07-29_11-08-40_epileptic-seizure_transitions.csv",
                       "2022-07-29_11-08-40_epileptic-seizure_confirmation.eps")

    # confusion_matrix("data/premature_closure_threshold-75", "confusion-matrix_premature_closure-75.txt")
    # plot_confusion_matrix("confusion-matrix_premature_closure-75.txt", "Confusion matrix Premature closure", save=False)

    # exploration_behaviour("data/premature_closure_threshold-75", "premature_closure-75.csv")
    # plot_exploration_behaviour("premature_closure-75.csv")

    # trajectory_length("data/premature_closure_threshold-50")

    # suffix = "availability_bias_ES"
    # feature_preference("data/availability_bias/syncope", "feature_counts_" + suffix + ".json")
    # plot_feature_preference("feature_counts_" + suffix + ".json")

    # plot_feature_difference()
